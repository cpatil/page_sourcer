require 'api_constraints'

Rails.application.routes.draw do
  # Api definition
  namespace :api, defaults: { format: :json },
  # constraints: { subdomain: 'api' },
  path: '/'  do
    scope module: :v1,
              constraints: ApiConstraints.new(version: 1, default: true) do
      resources :urls, only: [:show, :create]
    end
  end

  # You can have the root of your site routed with "root"
  root 'welcome#index'
end

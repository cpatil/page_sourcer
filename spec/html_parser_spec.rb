require 'spec_helper'

describe HtmlParser do
  let(:url) { 'http://test.com' }
  let(:body) { '   <html><head><title></head><body><div></div><br/><div><span>nested < and > </span></div></body></html>' }
  let(:golden) { "   &lt;<span class=\"sourcer-html sourcer\">html</span>&gt;&lt;<span class=\"sourcer-head sourcer\">head</span>&gt;&lt;<span class=\"sourcer-title sourcer\">title</span>&gt;&lt;/head&gt;&lt;<span class=\"sourcer-body sourcer\">body</span>&gt;&lt;<span class=\"sourcer-div sourcer\">div</span>&gt;&lt;/div&gt;&lt;<span class=\"sourcer-br sourcer\">br</span>/&gt;&lt;<span class=\"sourcer-div sourcer\">div</span>&gt;&lt;<span class=\"sourcer-span sourcer\">span</span>&gt;nested &lt; and &gt; &lt;/span&gt;&lt;/div&gt;&lt;/body&gt;&lt;/html&gt;" }
  let(:success_response) { double('RestClientResponse', code: 200, body: body) }
  subject { HtmlParser.new(url) }
  context "#initialize" do
    it "accepts a url and an optional timeout" do
      expect(subject.url).to eq(url)
      expect(subject.timeout).to eq(10)
    end
    it "initializes expected variables" do
      expect(subject.res).to eq([])
      expect(subject.err).to eq(false)
      expect(subject.tags_count).to be_a(Hash)
    end
    it "invokes fetch_source to fetch the content" do
      expect_any_instance_of(HtmlParser).to receive(:fetch_source)
      HtmlParser.new(url)
    end
  end

  context "#fetch_source" do
    describe "successful response" do
      before(:each) do
        expect(RestClient::Request).to receive(:execute) { success_response }
      end
      it "invokes RestClient to fetch the content" do
        expect(subject).to be
      end
      it "sets the code and body as expected" do
        expect(subject.code).to eq(200)
        expect(subject.body).to eq(body)
      end
    end
    describe "when an exception is raised" do
      it "traps exceptions as expected" do
        expect_any_instance_of(RestClient::ExceptionWithResponse).to receive(:http_code).and_return(422)
        expect_any_instance_of(RestClient::ExceptionWithResponse).to receive(:response).and_return('testing')
        expect(RestClient::Request).to receive(:execute).and_raise(RestClient::ExceptionWithResponse)
        expect(subject.code).to eq(422)
        expect(subject.body).to eq("testing")
      end
    end
  end

  context "#process" do
    describe "empty body" do
      before(:each) do
        expect(RestClient::Request).to receive(:execute) { double('empty response', code: 200, body: '') }
      end
      it "returns an empty string when body is empty" do
        expect(subject.process).to eq('')
      end
    end
    describe "with body" do
      before(:each) do
        expect(RestClient::Request).to receive(:execute) { success_response }
      end
      it "skips any leading spaces" do
        expect(body).to match(/^\s\s\s/)
        expect(subject.process).to match(/^\s\s\s/)
      end

      it "matches the tags and wraps as expected" do
        expect(subject.process).to eq(golden)
      end
    end
    describe "with custom body" do
      it "converts all < to &lt;" do
        expect(RestClient::Request).to receive(:execute) { double('customresponse', code: 200, body: '<<<<<<') }
        expect(subject.process).to eq("&lt;&lt;&lt;&lt;&lt;&lt;")
      end

      it "converts all > to &gt;" do
        expect(RestClient::Request).to receive(:execute) { double('customresponse', code: 200, body: '>>>>>>') }
        expect(subject.process).to eq("&gt;&gt;&gt;&gt;&gt;&gt;")
      end

      it "converts mixed < and > appropriately" do
        expect(RestClient::Request).to receive(:execute) { double('customresponse', code: 200, body: '<foo>><</foo>>') }
        expect(subject.process).to eq("&lt;<span class=\"sourcer-foo sourcer\">foo</span>&gt;&gt;&lt;&lt;/foo&gt;&gt;")
      end

      it "does not wrap non-html tags" do
        expect(RestClient::Request).to receive(:execute) { double('customresponse', code: 200, body: '<foo>><</foo>>') }
        expect(subject.process).to eq("&lt;<span class=\"sourcer-foo sourcer\">foo</span>&gt;&gt;&lt;&lt;/foo&gt;&gt;")
      end

      it "matches a single instance of a tag" do
        expect(RestClient::Request).to receive(:execute) { double('customresponse', code: 200, body: '<foo>><</foo>>') }
        expect(subject.process).to eq("&lt;<span class=\"sourcer-foo sourcer\">foo</span>&gt;&gt;&lt;&lt;/foo&gt;&gt;")
      end

      it "matches multiple instances of a tag" do
        expect(RestClient::Request).to receive(:execute) { double('customresponse', code: 200, body: '<foo>><<foo></foo></foo>>') }
        expect(subject.process).to eq("&lt;<span class=\"sourcer-foo sourcer\">foo</span>&gt;&gt;&lt;&lt;<span class=\"sourcer-foo sourcer\">foo</span>&gt;&lt;/foo&gt;&lt;/foo&gt;&gt;")
      end

      it "does not wrap comments" do
        expect(RestClient::Request).to receive(:execute) { double('customresponse', code: 200, body: '<foo>><<!foo></foo></foo>>') }
        expect(subject.process).to eq("&lt;<span class=\"sourcer-foo sourcer\">foo</span>&gt;&gt;&lt;&lt;!foo&gt;&lt;/foo&gt;&lt;/foo&gt;&gt;")
      end

      it "wraps the tag within spans" do
        expect(RestClient::Request).to receive(:execute) { double('customresponse', code: 200, body: '<foo>nested</foo>>') }
        expect(subject.process).to eq("&lt;<span class=\"sourcer-foo sourcer\">foo</span>&gt;nested&lt;/foo&gt;&gt;")
      end

      it "sets the class of the span as expected" do
        expect(RestClient::Request).to receive(:execute) { double('customresponse', code: 200, body: '<foo>nested</foo>>') }
        expect(subject.process).to match(/class="sourcer-foo sourcer">/)
      end

      it "counts the tags as expected" do
        expect(RestClient::Request).to receive(:execute) { double('customresponse', code: 200, body: '<foo>><<foo></foo></foo>>') }
        expect(subject.process).to eq("&lt;<span class=\"sourcer-foo sourcer\">foo</span>&gt;&gt;&lt;&lt;<span class=\"sourcer-foo sourcer\">foo</span>&gt;&lt;/foo&gt;&lt;/foo&gt;&gt;")
        expect(subject.tags_count['foo']).to eq(2)
      end

    end
  end

end

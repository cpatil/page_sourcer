FactoryGirl.define do
  factory :url_content do
    content { FFaker::HTMLIpsum.body }
    url
  end

end

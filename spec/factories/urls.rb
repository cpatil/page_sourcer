FactoryGirl.define do
  factory :url do
    url { FFaker::Internet.uri('https') }
    done false
    guid { FFaker::Guid.guid }
  end
end

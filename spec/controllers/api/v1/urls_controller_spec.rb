require 'rails_helper'

RSpec.describe Api::V1::UrlsController, type: :controller do

  let(:json_response) { JSON.parse(response.body) }
  let(:resource_response) { JSON.parse(response.body)["resource"] }
  let(:errors_response) { JSON.parse(response.body)["errors"] }

  describe "GET #show" do
    before(:each) do
      @url = FactoryGirl.create :url
      get :show, id: @url.id, format: :json
    end

    it "returns the information about the url" do
      expect(resource_response['url']).to eql @url.url
    end

    it { should respond_with 200 }

    it "returns current load conditions" do
      expect(json_response["poll-after"]).to eq(10)
      expect(json_response["server-load"]).to eq("light")
    end

    context "with etag support" do
      it "adds an etag header" do
        expect(response.headers["ETag"]).to be
      end
      it "responds with 304 when not changed" do
        request.env["HTTP_IF_NONE_MATCH"] = response.headers["ETag"]
        get :show, id: @url.id, format: :json
        response.code.should == "304"
      end
      context "if it has been updated" do
        before do
          @url.update_attributes!(done: true)
          request.env['HTTP_IF_NONE_MATCH'] = response.headers["ETag"]
        end
        it "returns a 200" do
          get :show, id: @url.id, format: :json
          response.code.should == "200"
          updated_response = JSON.parse(response.body)["resource"]
          expect(updated_response["done"]).to be_truthy
        end
      end
    end
  end

  describe "POST #create" do
    context "when is successfully created" do
      before(:each) do
        @url_attributes = FactoryGirl.attributes_for :url
        post :create, { url: @url_attributes }
      end

      it "renders the json representation for the url record just created" do
        expect(resource_response['url']).to eql @url_attributes[:url]
      end

      it { should respond_with 201 }

      it "returns current load conditions" do
        expect(json_response["poll-after"]).to eq(10)
        expect(json_response["server-load"]).to eq("light")
      end

    end

    context "when is not created" do
      before(:each) do
        @invalid_url_attributes = { url: nil, guid: nil }
        post :create, { url: @invalid_url_attributes }
      end

      it "renders an errors json" do
        expect(json_response).to have_key('errors')
      end

      it "renders the json errors on why the resource could not be created" do
        expect(errors_response['url']).to include "can't be blank"
      end

      it { should respond_with 422 }
    end
  end
end

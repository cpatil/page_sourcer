require 'rails_helper'

RSpec.describe Url, type: :model do
  let(:url) { FactoryGirl.build :url }
  subject { url }

  it { should respond_to(:guid) }
  it { should respond_to(:url) }
  it { should respond_to(:done) }
  it { expect(subject.done).to be_falsy }

  describe "associations" do
    it { should have_one(:url_content)}
    context "cascade" do
      before do
        url.save
        FactoryGirl.create :url_content, url: url
      end
      it "destroys url_content" do
        content = url.url_content
        url.destroy
        expect { UrlContent.find(content.id) }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end

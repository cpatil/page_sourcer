require 'rails_helper'

RSpec.describe UrlContent, type: :model do
  let(:url_content) { FactoryGirl.build :url_content }
  subject { url_content }

  it { should respond_to(:content) }
  it { should respond_to(:url) }
  it { should belong_to(:url) }
end

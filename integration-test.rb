#!/usr/bin/env ruby

require 'rubygems'
require 'rest-client'
require 'json'
require 'term/ansicolor'
require 'time'
require 'pp'
require 'set'
require 'digest/md5'
require 'cgi'
require 'nokogiri'
require 'byebug'
require 'open-uri'
require 'eventmachine'

HOST = "http://localhost:3000"

class Time
  def rfc3339; Time.now.utc.strftime("%FT%TZ") end
end

extend Term::ANSIColor
def puts_red s; print [red, s, clear, "\n"].join end
def puts_green s; print [green, s, clear, "\n"].join end
def puts_yellow_bg s; print [negative, s, clear, "\n"].join end

def test s; puts_yellow_bg "  #{s}" end

def get name, params={}
  puts "> GET /#{name}"

  munged_params = params.inject({}) { |h, (k, v)| h[k.to_s.gsub("_", "-")] = v; h }
  # pp munged_params
  v = RestClient.get "#{HOST}/#{name}", params: munged_params
  j = JSON.parse v
  # pp j
  [v, j]
end

def get_csrf name, params={}
  puts "> GET /#{name}"

  page = Nokogiri::HTML(RestClient.get "#{HOST}/#{name}")
  csrf = page.css("//meta[@name='csrf-token']/@content").first.value
  csrf
end

def delete name, params={}
  puts "> DELETE /#{name}"

  munged_params = params.inject({}) { |h, (k, v)| h[k.to_s.gsub("_", "-")] = v; h }
  pp munged_params
  RestClient.delete "#{HOST}/#{name}", params: munged_params
end

def post name, params={}
  puts "> POST /#{name}"
  munged_params = params.inject({}) { |h, (k, v)| h[k.to_s.gsub("_", "-")] = v; h }
  pp munged_params
  v = RestClient.post "#{HOST}/#{name}", munged_params
  j = JSON.parse v
  pp j
  [v, j]
end

def report_success! name
  puts_green "+ #{name}"
end

def failure_line_from_exception e, offset=0
  line_no = e.backtrace[e.backtrace.size - 1 - offset].scan(/:(\d+):/).first.first
  line = IO.readlines(__FILE__)[line_no.to_i - 1] ## why -1 ?!?!?!
  [line_no, line]
end

def report_failure! name, actual
  v = actual.to_s[0 ... 30]
  puts_red "- #{name} (got: #{v})"

  exc = nil
  begin
    raise ArgumentError
  rescue ArgumentError => e
    exc = e
  end

  line_no, line = failure_line_from_exception exc, 1
  puts
  puts "Test failure on line #{line_no}: #{line}"
  abort
end

def report! name, success, badval=nil
  if success
    report_success! name
  else
    report_failure! name, badval
  end
end

def nicely x
  x = x.inspect
  if (x.length > 20)
    x[0...20] + "\"..."
  else
    x
  end
end

def assert_raises exc
  name = "method raises #{exc}"
  ret = nil
  begin
    ret = yield
    report_failure! name, "(nothing)"
  rescue => e
    if exc === e
      report_success! name
      ret
    else
      report_failure! name, e.class
    end
  end
end

def assert_nothing_raised
  name = "method raises nothing"
  ret = nil
  begin
    ret = yield
    report_success! name
  rescue => e
    report_failure! name, e.class
  end
  ret
end

def assert_equal right, x
  name = "equals #{nicely right}"
  report! name, (x == right), x
end

def assert_not_equal right, x
  name = "does not equal #{nicely right}"
  report! name, (x != right), x
end

def assert_true v
  report! "is true", v, v.to_s
end

def assert_false v
  report! "is false", !v, v.to_s
end

def assert_not_nil v
  report! "is not nil", v, "nil"
end

def assert_not_blank v
  report! "is not blank", !v.nil? && !v.empty?, v.inspect
end

def assert_is_nil v
  report! "is nil", v.nil?, v
end

def assert_greater_than a, b
  report! "is greater than #{a}", (b > a), b
end

def assert_less_than a, b
  report! "is less than #{a}", (b < a), b
end

def assert_contains right, x
  name = "contains #{nicely right}"
  report! name, x.member?(right), x
  report! name, x.member?(right), x
end

def assert_doesnt_contain wrong, x
  name = "doesn't contain #{nicely wrong}"
  report! name, !x.member?(wrong), x
end

def assert_empty x
  report! "is empty", x.empty?, x.to_s
end

def assert_near_now time
  report! "is near now", (time - Time.now).abs < 2, time
end


def get_random_url
  ['http://yahoo.com', 'http://cnn.com', 'http://slack.com',
    'http://timesofindia.com', 
    'http://mercurynews.com', 'http://cbs.com', 'http://abc.com', 'http://nbc.com', 'http://hulu.com',
    'http://gmail.com', 'http://finance.google.com', 'http://news.google.com', 'http://facebook.com',
    'http://twitter.com', 'http://pinterest.com'
#     'http://localhost:8094?sleep=100&url=http%3A%2F%2Fyahoo.com',
#     'http://localhost:8094?sleep=100&url=http%3A%2F%2Fsfgate.com',
#     'http://localhost:8094?sleep=100&url=http%3A%2F%2Faol.com',
#     'http://localhost:8094?sleep=100&url=http%3A%2F%2Ffoxnews.com'
  ].sample
end

def run_show_after_5(id)
  EventMachine::Timer.new(5) do
    r, j = get "/urls/#{id}"
    if j["url"]["in_flight"]
      run_show_after_5(id)
    else
      pp "DONE #{id}"
    end
  end
end

def run_integration_test
  EM.run {
    EM::Iterator.new(1..10000, 100).each do |num, iter|
      test 'fetch the front page for CSRF token'
      csrf = get_csrf '/'
      
      test "Post a url"
      r, j = post "/urls", url: {url: get_random_url}, authenticity_token: csrf
      
      url_id = j["url"]["id"]
      run_show_after_5(url_id)
      EventMachine::Timer.new(10) {iter.next}
    end
  }
end

############ execution starts here ############

if __FILE__ == $0
  begin
    run_integration_test
    puts "Success!"
  rescue => e
    line_no, line = failure_line_from_exception e, 1
    puts
    puts "Test exception (#{e.class}) on line #{line_no}: #{line}"
    puts e.message
    puts e.backtrace
    puts
    puts "FAILED: exception on line #{line_no}."
  end
end

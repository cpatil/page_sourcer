// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
(function () {
    var Url = window.Url = {
        init: function () {
            $('#spinner-wrapper').hide();
            $('#panel-wrapper').hide();
            $("#new_url").on('submit', Url.create);
            $('.first-input').focus();
        },
        create: function () {
            // this is the form here
            // start the spinner
            $('#panel-wrapper').hide();
            Url.startSpinner();
            Url.url_resource_path = $(this).attr('action');
            $.ajax({
                type: "POST",
                url: Url.url_resource_path, //sumbits it to the given url of the form
                data: {url: {url: $('input#url').val()}},
                dataType: "JSON"
            }).success(Url.on_create_success).error(Url.on_create_error).complete(Url.on_create_complete);
            return false; // prevents normal behaviour
        },
        url_resource: null,
        server_load_lookup: {
            'light': 'text-success',
            'medium': 'text-warning',
            'heavy': 'text-danger'
        },
        on_create_success: function (data) {
            Url.url_resource = data['url'];
            var meta = data['meta'] || {};
            var poll_after = meta['poll-after'] || 5;
            var current_server_load = meta['server-load'] || 'light';
            Url.setServerLoadClass(Url.server_load_lookup[current_server_load]);
            Url.countdown(poll_after, current_server_load);
        },
        on_create_error: function (xhr, status, err) {
            console.log(status);
            console.log(err);
        },
        on_create_complete: function () {
            //Url.stopSpinner();
        },
        setServerLoadClass: function (load_class) {
            if (Url.current_server_load_class)
                $('#spinner-message').removeClass(Url.current_server_load_class);
            Url.current_server_load_class = load_class;
            $('#spinner-message').addClass(load_class);
        },
        poll: function () {
            $.ajax({
                type: "GET",
                url: Url.url_resource_path + '/' + Url.url_resource['id'],
                dataType: "JSON"
            }).success(Url.on_show_success).error(Url.on_show_error).complete(Url.on_show_complete);
        },
        on_show_success: function (data) {
            Url.url_resource = data['url'];
            if (Url.url_resource['in_flight']) {
                var meta = data['meta'] || {};
                var poll_after = meta['poll-after'] || 5;
                var current_server_load = meta['server-load'] || 'light';
                Url.setServerLoadClass(Url.server_load_lookup[current_server_load]);
                Url.countdown(poll_after, current_server_load);
            } else {
                Url.renderResult();
            }
        },
        on_show_error: function (xhr, status, err) {
            Url.stopSpinner();
            $('#result-panel').addClass('panel-danger');
            $('.panel-body').addClass('text-danger').text(err);

            // show result panel
            $('#panel-wrapper').removeClass('hidden');
            $('#panel-wrapper').show();
        },
        on_show_complete: function () {
            //Url.stopSpinner();
        },
        renderResult: function () {
            Url.stopSpinner();
            data = Url.url_resource;
            // reinit
            $('#result-panel').removeClass('panel-danger').removeClass('panel-success');
            $('.panel-body').removeClass('text-danger')

            //
            if (data['successful']) {
                $('#result-panel').addClass('panel-success');
                $('.panel-body').html(data['url_content']['content']);
                $('.sourcer-summary-tag').on('click', function (evt) {
                    $('.sourcer-summary-tag-hilited').removeClass('btn-success').addClass('btn-link').removeClass('sourcer-summary-tag-hilited');
                    $('.sourcer').removeClass('btn-success').removeClass('btn').removeClass('text-uppercase');
                    target = $(evt.target);
                    target.removeClass('btn-link').addClass('btn-success').addClass('sourcer-summary-tag-hilited');
                    tag = target.data('tag');
                    $('.sourcer-' + tag).addClass('btn').addClass('btn-success').addClass('text-uppercase');
                });
            } else {
                $('#result-panel').addClass('panel-danger');
                $('.panel-body').addClass('text-danger').text(data['errmsg']);
            }

            // show result panel
            $('#panel-wrapper').removeClass('hidden');
            $('#panel-wrapper').show();
        },
        countdown: function (seconds, current_load) {
            current_load = current_load || 'light';
            if (Url.countDownTimer) {
                window.clearInterval(Url.countDownTimer);
            }
            if (seconds > 1) {
                $('#spinner-message').text('Polling after ' + seconds + ' seconds (current server load: ' + current_load + ')');
                Url.countDownTimer = window.setInterval(function () {
                    Url.countdown(seconds - 1, current_load)
                }, 1000);
            } else {
                $('#spinner-message').text('Polling now');
                Url.poll();
            }
        },
        startSpinner: function () {
            var canvas = document.getElementById('spinner');
            var context = canvas.getContext('2d');
            var start = new Date();
            var lines = 16,
                cW = context.canvas.width,
                cH = context.canvas.height;

            var draw = function () {
                var rotation = parseInt(((new Date() - start) / 1000) * lines) / lines;
                context.save();
                context.clearRect(0, 0, cW, cH);
                context.translate(cW / 2, cH / 2);
                context.rotate(Math.PI * 2 * rotation);
                for (var i = 0; i < lines; i++) {

                    context.beginPath();
                    context.rotate(Math.PI * 2 / lines);
                    context.moveTo(cW / 10, 0);
                    context.lineTo(cW / 4, 0);
                    context.lineWidth = cW / 30;
                    context.strokeStyle = "rgba(180,180,180," + i / lines + ")";
                    context.stroke();
                }
                context.restore();
            };
            window.drawTimer = window.setInterval(draw, 1000 / 30);
            $('#spinner-wrapper').removeClass('hidden');
            $('#spinner-wrapper').show();
        },
        stopSpinner: function () {
            clearInterval(window.drawTimer);
            $('#spinner-wrapper').hide();
        }
    }

    Url.init();

})();

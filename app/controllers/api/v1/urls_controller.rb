require 'current_app_load'
class Api::V1::UrlsController < ApplicationController
  respond_to :json

  def show
    url = Url.where(id: params[:id]).includes(:url_content).first
    current_load = CurrentAppLoad.new
    fresh_when(:etag => [url, current_load.server_load_code], :last_modified => url.updated_at.utc, :public => true) and return
    # render json: { "resource" => url, "poll-after" => current_load.poll_after, "server-load" => current_load.server_load_code }.to_json, status: 200
    render json: url, status: 200, location: [:api, url]
  end

  def create
    url = Url.new(url_params.merge(guid: SecureRandom.uuid))
    current_load = ::CurrentAppLoad.new
    if url.save
      ::PageSourcerJob.new.async.perform(url.id)
      render json: url, status: 201, location: [:api, url]
    else
      render json: { errors: url.errors }, status: 422
    end
  end

  private

  def url_params
    params.require(:url).permit(:url)
  end
end

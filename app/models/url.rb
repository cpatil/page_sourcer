require 'uri'

class Url < ActiveRecord::Base
  has_one :url_content, dependent: :destroy

  validates :url, :guid, presence:true
  # validates_uniqueness_of :guid
  validate :url_validate

  def url_validate
    uri = URI.parse(url)
    errors.add(:url, 'is invalid') unless uri.kind_of?(URI::HTTP)
  rescue URI::InvalidURIError
    errors.add(:url, 'is invalid')
    false
  end

end

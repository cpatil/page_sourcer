require 'restclient'
require 'html_parser'
class PageSourcerJob
  include SuckerPunch::Job
  workers 4

  def perform(url_id)
    # update the url content
    ActiveRecord::Base.connection_pool.with_connection do
      begin
        url = ::Url.find(url_id)
        parser = ::HtmlParser.new(url.url, url.timeout)
        unless parser.err
          url.create_url_content(content: ApplicationController.new.render_to_string(partial: 'api/v1/urls/content',
                                                                                     locals: {content: parser.process, tags_count: parser.tags_count}).encode('UTF-8', {:invalid => :replace, :undef => :replace, :replace => '?'}
                                 ))
        end

        url.update_attributes(in_flight: false, successful: !parser.err, code: parser.code, errmsg: parser.err ? parser.body : nil)
      rescue Exception => e
        url.update_attributes(in_flight: false, successful: false, code: -1, errmsg: ['Error while processing: ' + e.message].join(''));
      end
    end
  end

end

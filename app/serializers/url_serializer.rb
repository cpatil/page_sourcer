class UrlSerializer < ActiveModel::Serializer
  attributes :id, :successful, :in_flight, :errmsg, :code
  has_one :url_content

  def meta
    if object.successful
      {}
    else
      current_load = CurrentAppLoad.new
      {"poll-after" => current_load.poll_after, "server-load" => current_load.server_load_code}
    end
  end
end

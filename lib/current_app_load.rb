class CurrentAppLoad
  def initialize
    @num_urls_in_flight = Url.where(in_flight: true).count
  end

  def poll_after
    @num_urls_in_flight > 0 ? @num_urls_in_flight * 3 : 2
  end

  def server_load_code
    case
      when @num_urls_in_flight < 3
        :light
      when @num_urls_in_flight < 5
        :medium
      else
        :heavy
    end
  end
end
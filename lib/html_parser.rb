require 'strscan'
require 'rest-client'
class HtmlParser
  attr_accessor :res, :url, :code, :body, :err, :timeout, :tags_count

  def initialize(url, timeout=10)
    @url = url
    @res = []
    @err = false
    @tags_count = Hash.new(0)
    @timeout = timeout
    fetch_source
  end

  def fetch_source
    res = RestClient::Request.execute({method: :get, url: @url, timeout: timeout, verify_ssl: false})
    @code = res.code
    @body = res.body
  rescue RestClient::ExceptionWithResponse => e
    @err = true
    @code = e.http_code || -1
    @body = e.response || e.message
  rescue Exception => e
    # RestClient::ServerBrokeConnection, SocketError, RestClient::Exception, Errno::ECONNREFUSED
    @err = true
    @code = -1
    @body = e.message
  end

  def process
    return "" unless @body && @body.length > 0
    @buffer = StringScanner.new(@body)
    skip_spaces_if_any
    while (until_tag_beginning = @buffer.scan_until(/</))
      @res += [(until_tag_beginning[0..-2]).gsub(/>/,'&gt;')]
      @res += ['&lt;']
      if tag = @buffer.scan(/([\w-]+)/)
        # found a tag
        # lets wrap the match in span
        @tags_count[tag] += 1
        @res += [%Q!<span class="sourcer-#{tag} sourcer">!, tag, '</span>']
      else
        next
      end

      until_tag_close = @buffer.scan_until(/>/)
      @res += [(until_tag_close[0..-2]).gsub(/</,'&lt;'), '&gt;']
    end
    append_rest_of_string
    @res.compact.join.gsub(/\n|\r\n/, '<br/>')
  end

  def skip_spaces_if_any
    @res += [@buffer.scan(/\s+/)]
  end

  def append_rest_of_string
    @res += [@buffer.rest.gsub(/>/,'&gt;').gsub(/</, '&lt;')]
  end

end

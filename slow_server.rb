#!/usr/bin/env ruby
require 'eventmachine'
require 'em-http-server'
require 'rest-client'
require 'cgi'

CONTENT = '1' * 1000

class HTTPHandler < EM::HttpServer::Server
  
  def process_http_request
    puts  ["%10.6f" % Time.now, ' - ', @http.inspect, '  -   ', @http_query_string] * ''
    params = Hash[*((@http_query_string || '').split("&").map{|s| s.split("=")}.flatten)]
    @timer = EventMachine::Timer.new (params['sleep'] || 2), proc {
      response = EM::DelegatedHttpResponse.new(self)
      response.status = params['code'] || 200
      response.content_type 'text/html'
      if params['url']
        response.content = RestClient::Request.execute({method: :get, url: CGI.unescape(params['url']), verify_ssl: false}).body
      else
        response.content = CONTENT
      end
      response.send_response
    }    
  end

  def unbind
    @timer.cancel unless @timer.nil? rescue nil
  end
  
  def http_request_errback e
    # printing the whole exception
    puts e.inspect
  end
  
end

EM::run do
  EM::start_server("0.0.0.0", 8094, HTTPHandler)
  puts "Listening on 0.0.0.0:8094"
end



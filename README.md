# Page Sourcer #

## Summary: ##
Create a simple web app where users can enter a URL of a page to fetch. The web app fetches the HTML of the page and displays the source to the user. A summary of the document is displayed, listing which tags are present in the HTML and how many of each tag. Clicking on the name of each tag in the summary will highlight the tags in the source code view.

## Solution: ##
**Some salient points of the architecture:**

* Rails application with in-process asynchronous batch job support for the actual fetch of the url content. Incidentally, a node.js solution would have been more optimal to use here due to the asynchronous nature of the solution, but I was curious to see how a single Rails process can scale for this prototype.
* Restful resource endpoints (/urls) : #create and #show
* Every url submission is a new job, this allows the user to open multiple tabs and issue independent requests in the same browser window.
* Once a url is submitted, an asynchronous (in-process) job (PageSourcerJob) is scheduled that fetches the content and then processes it (HtmlParser). Once the job is complete, the url record is updated in the database with the results. Thus the database is used for synchronization in this prototype.
* The frontend polls the url#show api endpoint to monitor the completion of the job, Etags are used to optimize resource usage and minimize network bandwidth usage for these poll requests. The polling aspect can be further optimized away by using a websocket framework like socket.io instead.
* Based on the number of url fetch requests in flight, a simple server-load metric (CurrentAppLoad) is computed. Based on this server load, additional meta information (poll-after and server-load) are also returned with each request. This allows the frontend client to throttle the poll requests as well. An additional optimization that also could have been implemented is client side exponential backoff besides using the poll-after provided by server.
* To simulate the server loading as well as timeout support, open a new browser window and open upto 6 tabs to http://loop6.com:8095.
Use the following url to implement a slow request  (see slow_server.rb):
http://loop6.com:8094?sleep=60&url=http%3A%2F%2Fyahoo.com
(this may not be available at all times) 

As the number of url submissions increase and the number of url fetches in flight increase, the server starts returning a server-load metric that can be used to provide user feedback and/or limit additional url submissions.

There is some testing support for the api endpoints.

https://bitbucket.org/cpatil/page_sourcer
You can try the app out at 
http://loop6.com:8095/
To simulate load for the app, you can open multiple tabs (at least 6) and use another service that I wrote to delay the fetching of the url. 
http://loop6.com:8094?sleep=60&url=http%3A%2F%2Fyahoo.com

Notes:

* Add api endpoint /url (create and show) to submit the url and fetch its content. Once the contents are fetched, the result can be fetched using show. On creation, the response contains a poll-after attr that is sensitive to current url requests in flight, and server-load attr
* Use sucker_punch to async the processing and use custom etag in show for optimizing polling. Add poll-after and server-load parameters here as well
* Add / route to return a page that has a form to accept a url and an optional timeout and submits to the url#create api ep.
* Add Js that manages the creation of the url request and then polls to check for the results and once fetched, renders the summarizer content
* Use http://localhost:8080?sleep=100&url=http%3A%2F%2Fyahoo.com to test for mock server load (bundle exec ruby slow_server.rb)
class AddTimeoutToUrl < ActiveRecord::Migration
  def change
    add_column :urls, :timeout, :integer
  end
end

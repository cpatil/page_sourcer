class AddMoreAttrsToUrl < ActiveRecord::Migration
  def change
    add_column :urls, :errmsg, :string
    add_column :urls, :code, :integer
    add_column :urls, :successful, :boolean, default: false
  end
end

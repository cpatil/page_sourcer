class CreateUrls < ActiveRecord::Migration
  def change
    create_table :urls do |t|
      t.string :url, index: true, null: false
      t.string :guid, index:true

      t.timestamps null: false
    end
  end
end

class CreateUrlContents < ActiveRecord::Migration
  def change
    create_table :url_contents do |t|
      t.text   :content
      t.references :url, index: true

      t.timestamps null: false
    end
    add_foreign_key :url_contents, :urls
  end
end

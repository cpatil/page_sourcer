class AddInFlightToUrl < ActiveRecord::Migration
  def change
    add_column :urls, :in_flight, :boolean, default: true, index: true
  end
end

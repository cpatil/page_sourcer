# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150711161032) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "url_contents", force: :cascade do |t|
    t.text     "content"
    t.integer  "url_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "url_contents", ["url_id"], name: "index_url_contents_on_url_id", using: :btree

  create_table "urls", force: :cascade do |t|
    t.string   "url",                        null: false
    t.string   "guid"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "in_flight",  default: true
    t.string   "errmsg"
    t.integer  "code"
    t.boolean  "successful", default: false
    t.integer  "timeout"
  end

  add_index "urls", ["guid"], name: "index_urls_on_guid", using: :btree
  add_index "urls", ["url"], name: "index_urls_on_url", using: :btree

  add_foreign_key "url_contents", "urls"
end
